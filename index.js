const fs = require('fs')
const path = require('path')
const npmPackage = require('./package.json')

module.exports = {
  name: 'b-ber-theme-ccc-issue-26-two-ears-and-one-mouth',
  entry: path.join(__dirname, 'application.scss'),
  fonts: (() =>
    fs
      .readdirSync(path.join(__dirname, 'fonts'))
      .filter(a => /\.(otf|ttf|woff2?|eot)/i.test(path.extname(a))))(),
  images: [],
  npmPackage,
}
