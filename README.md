# `b-ber-theme-ccc-issue-26-two-ears-and-one-mouth`

A Triple Canopy specific b-ber theme that extends [b-ber-theme-ccc](https://gitlab.com/canopycanopycanopy/b-ber-themes/b-ber-theme-ccc)

## Install

```
$ cd my-project
$ npm init -y
$ npm install -S git+https://gitlab.com/canopycanopycanopy/b-ber-themes/b-ber-theme-ccc-issue-26-two-ears-and-one-mouth.git
$ bber theme set b-ber-theme-ccc-issue-26-two-ears-and-one-mouth
```

## Update

In order to pull down new commits to the repo, follow these instructions in individual b-ber projects.

```
$ npm update b-ber-theme-ccc-issue-26-two-ears-and-one-mouth
```
